$(document).ready(function () {
    showBar('', 2500);

    $('#use_functional').on('click', function () {
        showBar('', 'slow');
        $("input[name='new_hashtag']").focus();
    });

    $('.close').on('click', function () {
        hideBar($(this));
    });
    $('.open').on('click', function () {
        showBar($(this), 'slow');
    });

    $(window).resize(function () {
        $('.close').unbind('click');
        $('.close').on('click', function () {
            hideBar($(this));
        });
        $('.open').unbind('click');
        $('.open').on('click', function () {
            showBar($(this), 'slow');
        });
    });
});

/* SIDEBAR */
function hideBar(elem) {
    $(elem).unbind('click');
    //sidebar left
    var sidebarWidth = $('.sidebar').width();
    $(".sidebar").animate({"left":'-' + sidebarWidth + "px"}, "slow", (function() {
        $('.open').show();
    }));

    $(elem).on('click', function () {
        hideBar(elem);
    });
}
function showBar(elem, speed) {
    if(elem !== '') $(elem).unbind('click');
    //sidebar right
    $('.open').hide();
    var sidebarWidth = $('.sidebar').width();
    $(".sidebar").animate({"left":"0px"}, speed);

    if(elem !== '') {
        $(elem).on('click', function () {
            showBar(elem);
        });
    }
}

/* construct html to paste */
function constructHashtag(hashtag) {
    var html = '<div class="hashtag">' +
        '<a href="https://www.instagram.com/explore/tags/' + hashtag + '" target="_blank">#' + hashtag + '</a>' +
        '<div onclick="removeHashtag(this)" class="remove_hashtag"></div>' +
        '<input type="hidden" name="hashtags[]" value="' + hashtag + '" />' +
        '</div>';
    return html;
}
function constructPost(post) {
    var html = '<div class="post">';
    html += '<div class="post_body">';
    html += '<a href="' + post['href'] + '"><img src="' + post['midsize'] + '" /></a>';
    html += '</div>';
    html += '<div class="post_footer">';
    html += '<p>' + post['text'] + '</p>';
    html += '</div>';
    html += '</div>';
    return html;
}

/* hashtags functions */
function addHashtag(elem) {
    if(hashtagsValidate(false)) {
        var hashtag = $(elem).siblings('input').val();
        $(this).siblings('input').val('');
        $('.hash_posts').append(constructHashtag(hashtag));
        $("input[name='new_hashtag']").val('');
    }
}
function removeHashtag(elem) {
    $(elem).parent().remove();
}
function getHashtags() {
    var hashtags = '';
    $('.hashtag').children('input').each(function (key, val) {
        hashtags += (hashtags !== '' ? ',' : '') + $(val).val();
    });

    return hashtags
}
function hashtagsValidate(minimal) {
    if(minimal && $('.hashtag').length < minimal){
        alert('Вы не добавили ни одного хештега! Для добавления нажмите + рядом с полем для ввода.');
        return false;
    }

    if($('.hashtag').length <= 5) {
        return true;
    } else{
        alert('Максимум 5 хештегов');
        return false;
    }
}
/* ajax dinamical functional */
function uploadPosts() {
    if(hashtagsValidate(1)) {
        var hashtags = getHashtags();

        $.ajax({
            url: 'index.php?method=uploadPosts',
            type: 'get',
            dataType: 'json',
            data: {hashtags: hashtags},
            beforeSend: function () {
                $('#blur').show();
            },
            success: function (json) {
                $('#blur').hide();
                if (json['hashtags'] && json['posts']) {
                    var html = '';
                    $.each(json['hashtags'], function (hashtag, values) {
                        html += constructHashtag(hashtag);
                    });
                    $('.hash_posts').html(html);

                    var html = '<div class="posts">';
                    $.each(json['posts'], function (key, post) {
                        html += constructPost(post);
                    });
                    html += '</div><div class="show_more">' +
                                '<button onclick="showMorePosts()" type="button">ПОКАЗАТЬ ЕЩЁ</button>' +
                            '</div>';
                    $('.content').children('.container').html(html);
                }
            },
            error: function (json) {
                console.log(json);
                alert('Something is going wrong!');
            }
        });
    }
}

function showMorePosts() {
    var hashtags = getHashtags();
    $.ajax({
        url: 'index.php?method=showMore',
        type: 'get',
        dataType: 'json',
        data: {hashtags: hashtags},
        beforeSend: function () {
            $('#blur').show();
        },
        success: function (json) {
            $('#blur').hide();
            if(json['posts']) {
                var html = '';
                $.each(json['posts'], function (key, post) {
                    html += constructPost(post);
                });
                $('.posts').append(html);
            } else {
                $('.show_more').children('button').hide();
            }
        },
        error: function (json) {
            console.log(json);
            alert('Something is going wrong!');
        }
    });
}