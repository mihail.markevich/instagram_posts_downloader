<?php

/**
Its just a simple example to show
 how the instagram-posts downloading functional works
 */

if(file_exists('controllers/instaController.php')) {
    session_start();

    require_once ('controllers/instaController.php');

    $insta = new InstaController();

    if(isset($_GET['method'])) {
        $method = $_GET['method'];
        if(method_exists($insta,$method)){
            $insta->$method();
        } else {
            return false;
        }

    } else {
        $insta->start();
    }
}