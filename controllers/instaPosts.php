<?php

Class InstaPosts {
    public $handle = null;
    public $code;
    public $body;
    public $headers;
    public $maxRequests = 0;
    public $hashtags_control = array();
    public $hashtags_posts   = array();
    public $path = 'posts/';
    public $log = 'logs/log.txt';

    const MEDIA_JSON_BY_TAG_BY_HASH = 'https://www.instagram.com/graphql/query/?query_hash=ded47faa9a1aaded10161a2ff32abb6b&variables={variables}';
    const MEDIA_JSON_BY_TAG = 'https://www.instagram.com/explore/tags/{tag}/?__a=1';
    const MAX_ID_LINK = '&max_id={end_cursor}';
    const MEDIA_OWNER = 'https://i.instagram.com/api/v1/users/{user_id}/info/';

    /** this user-agent is helpful to escape the instagram rules */
    const USER_AGENT = 'Mozilla/5.0 (iPhone; CPU iPhone OS 12_3_1 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Mobile/15E148 Instagram 105.0.0.11.118 (iPhone11,8; iOS 12_3_1; en_US; en-US; scale=2.00; 828x1792; 165586599)';

    public function __construct(){
        header('Content-Type: text/html; charset=utf-8');
        ini_set('max_execution_time','480');
        if(!is_dir($this->path)){
            mkdir($this->path, 0777);
        }
        if(!is_file($this->log)){
            fopen($this->log, 'w');
            @chmod($this->log, 0777);
        }
    }

    public function getPosts($tags){
        if(isset($_SESSION['hashtags_control']) && $_SESSION['hashtags_control']){
            $this->hashtags_control = $_SESSION['hashtags_control'];
            $this->hashtags_posts   = $_SESSION['hashtags_posts'];
        }

        if (!empty($tags)) {
            foreach ($tags as $key => $tag) {
                if (!empty($tag)) {
                    $this->getMediasByTagInstagram($tag);
                }
            }
        } else {
            return false;
        }

        $_SESSION['hashtags_control'] = $this->hashtags_control;
        $_SESSION["hashtags_posts"]   = $this->hashtags_posts;

        return array('hashtags' => $this->hashtags_control, 'posts' => $this->hashtags_posts);
   }

    public function getMediasByTagInstagram($hashtag){
        $tag = str_replace('#', '', $hashtag);

        //End cursor - its the next page of Instagram-posts. By one execute we can get only one page
        if(isset($this->hashtags_control[$tag]['end_cursor']) && $this->hashtags_control[$tag]['end_cursor']){
            $end_cursor = $this->hashtags_control[$tag]['end_cursor'];
        } else {
            $end_cursor = '';
        }

        $result = $this->curlGetPosts($this->getMediasJsonByTagLink($tag, $end_cursor))['graphql']['hashtag'];
        if($result) {
            $posts = $result['edge_hashtag_to_media']['edges'];
            $all_tag_posts = $result['edge_hashtag_to_media']['count'];
            $end_cursor = $result['edge_hashtag_to_media']['page_info']['end_cursor'];

            //Updating data hashtags data
            $this->hashtagControl($tag, count($posts), $all_tag_posts, $end_cursor);
            file_put_contents($this->log, 'tag: ' . $tag . ', posts: ' . count($posts) . "\r\n", FILE_APPEND);

            if (!empty($posts)) {
                foreach ($posts as $key => $post) {
                    $post_id = $post['node']['id'];
                    $this->hashtags_posts[$post_id] = array(
                        'id' => $post_id,
                        'tag' => $tag,
                        'href' => 'https://www.instagram.com/p/' . $post['node']['shortcode'],
                        'code' => $this->code,
                        'text' => preg_replace('/#/', ' #', (isset($post['node']['edge_media_to_caption']['edges'][0]['node']['text']) ? $post['node']['edge_media_to_caption']['edges'][0]['node']['text'] : '')),
                        'fullsize' => (($post['node']['is_video'] == true) ? $post['node']['thumbnail_src'] : $post['node']['display_url']),
                        'is_video' => (($post['node']['is_video'] == true) ? 1 : 0),
                        'midsize' => $post['node']['thumbnail_resources'][3]['src'],
                        'minsize' => $post['node']['thumbnail_resources'][1]['src'],
                        'likesCount' => $post['node']['edge_liked_by'],
                        'commentsCount' => $post['node']['edge_media_to_comment'],
                        'user' => array(),//$this->getUser($post['node']['owner']['id']),
                        'date_added' => date('d.m.Y H:i', $post['node']['taken_at_timestamp'])
                    );
                }
            }
        } else{
            //Removing unsuccessful tags from data
            unset($this->hashtags_control[$hashtag]);
        }

    }

    public function hashtagControl($tag, $count_posts, $all_tag_posts, $end_cursor){
        $count_posts = (isset($this->hashtags_control[$tag]['count']) ? ($this->hashtags_control[$tag]['count'] + $count_posts) : $count_posts);
        if ($count_posts >= $all_tag_posts || !$end_cursor) {
            $count_posts = 0;
            $end_cursor  = '';
        }

        $this->hashtags_control[$tag] = array(
            'count'         => $count_posts,
            'all_tag_posts' => $all_tag_posts,
            'end_cursor'    => $end_cursor
        );
    }


    public function getMediasJsonByTagLink($tag, $end_cursor = ''){
        $url = str_replace('{tag}', $tag, static::MEDIA_JSON_BY_TAG);
        if($end_cursor) {
            $url .= str_replace('{end_cursor}', $end_cursor, static::MAX_ID_LINK);
        }

        return $url;
    }

    public function curlGetPosts($url){
        $this->handle = curl_init();
        $curl_base_options = [
            CURLOPT_URL => $this->encodeUrl($url),
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_FOLLOWLOCATION => false,
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_HEADER => true,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_SSL_VERIFYHOST =>  0 ,
            CURLOPT_ENCODING => ''
        ];
        curl_setopt_array($this->handle, $curl_base_options);

        $response	 = $this->curl_redir_exec($this->handle);
        $info        = curl_getinfo($this->handle);
        $header_size = $info['header_size'];
        $body        = substr($response, $header_size);
        $this->code  = $info['http_code'];

        curl_close($this->handle);

        return json_decode($body, true, 512, JSON_BIGINT_AS_STRING);
    }

    public function encodeUrl($url){
        $url_parsed = parse_url($url);
        $scheme = $url_parsed['scheme'] . '://';
        $host   = $url_parsed['host'];
        $port   = (isset($url_parsed['port']) ? $url_parsed['port'] : null);
        $path   = (isset($url_parsed['path']) ? $url_parsed['path'] : null);
        $query  = (isset($url_parsed['query']) ? $url_parsed['query'] : null);
        if ($query !== null) {
            $query = '?' . http_build_query($this->getArrayFromQuerystring($query));
        }
        if ($port && $port[0] !== ':') {
            $port = ':' . $port;
        }
        $result = $scheme . $host . $port . $path . $query;
        return $result;
    }

    public function getArrayFromQuerystring($query){
        $query = preg_replace_callback('/(?:^|(?<=&))[^=[]+/', function ($match) {
            return bin2hex(urldecode($match[0]));
        }, $query);
        parse_str($query, $values);
        return array_combine(array_map('hex2bin', array_keys($values)), $values);
    }

    public function curl_redir_exec($ch){
        $data = curl_exec($ch);
        $http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        if ($http_code == 301 || $http_code == 302) {
            $matches = array();
            preg_match('/Location:(.*?)\n/', $data, $matches);
            $url = @parse_url(trim(array_pop($matches)));
            if (!$url){
                return $data;
            }
            $last_url = parse_url(curl_getinfo($ch, CURLINFO_EFFECTIVE_URL));
            if (!$url['scheme']) $url['scheme'] = $last_url['scheme'];
            if (!$url['host']) 	$url['host'] = $last_url['host'];
            if (!$url['path']) 	$url['path'] = $last_url['path'];
            $new_url = $url['scheme'] . '://' . $url['host'] . $url['path'];
            curl_setopt($ch, CURLOPT_URL, $new_url);
            return $this->curl_redir_exec($ch);
        } else {
            return $data;
        }
    }

    /**
       Instagram blocking that type of getting data, if its too much, but you can try this one on 88 line
    */
    public function getUser($user_id){
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, str_replace('{user_id}',$user_id,static::MEDIA_OWNER));
        curl_setopt($curl, CURLOPT_USERAGENT, static::USER_AGENT);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $result = json_decode(curl_exec($curl), 1);
        curl_close($curl);

        if(!isset($result['user'])){
            $result = $this->getUser($user_id);
        }

        return isset($result['user']) ? $result['user'] : array();
    }
}
?>