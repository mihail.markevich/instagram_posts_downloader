<?php

class InstaController {
    public $data = array();
    public $instagram;
    public $dir = 'posts/';
    public $posts;
    public $log;
    public $limit = 8;

    public function __construct(){
        if (file_exists('controllers/instaPosts.php')){
            require_once('controllers/instaPosts.php');
            $this->instagram = new InstaPosts();
        } else {
            var_dump("Library file not exists!");die();
        }

        if (!is_dir($this->dir)){
            mkdir($this->dir);
        }
    }

    public function start(){
        $this->data['hashtags_control'] = array();
        $this->data['hashtags_posts']   = array();

        if((isset($_SESSION['hashtags_control']) && $_SESSION['hashtags_control']) && (isset($_SESSION['hashtags_posts']) && $_SESSION['hashtags_posts'])) {
            $this->data['hashtags_control'] = $_SESSION['hashtags_control'];
            $this->data['hashtags_posts']   = $_SESSION['hashtags_posts'];
        } else {
            $_SESSION['hashtags_control'] = array();
            $_SESSION['hashtags_posts']   = array();
        }

        //Render
        extract($this->data);
        require_once ('view/template/index.html');
    }

    /**
    Ajax methods
     */
    public function uploadPosts(){
        $json = array();

        if(isset($_SESSION['hashtags_control']) && $_SESSION['hashtags_control'] && isset($_GET['hashtags']) && $_GET['hashtags']) {
            $hashtags = array();
            foreach (explode(',', $_GET['hashtags']) as $post_hashtag){
                $hashtags[$post_hashtag] = $post_hashtag;
            }
            ksort($hashtags);

            $hashtags_session = array_keys($_SESSION['hashtags_control']);
            ksort($hashtags_session);

            if(implode(',', $hashtags_session) == implode(',', $hashtags)){
                exit(json_encode($json));
            }
        }

        $hashtags = array();
        foreach (explode(',',$_GET['hashtags']) as $post_hashtag) {
            $hashtags[$post_hashtag] = $post_hashtag;
        }
        ksort($hashtags);

        $_SESSION['hashtags_control'] = array();
        $_SESSION['hashtags_posts']   = array();

        $json = $this->instagram->getPosts(array_keys($hashtags));

        exit(json_encode($json));
    }

    public function showMore(){
        $json = array();

        if(isset($_SESSION['hashtags_control']) && $_SESSION['hashtags_control'] && isset($_GET['hashtags']) && $_GET['hashtags']) {
            $hashtags = array();
            foreach (explode(',', $_GET['hashtags']) as $post_hashtag) {
                $hashtags[$post_hashtag] = $post_hashtag;
            }
            ksort($hashtags);

            $posted = $_SESSION['hashtags_posts'];
            $posts = $this->instagram->getPosts(array_keys($hashtags));
            $new_posts = array();
            foreach ($posts['posts'] as $post_id => $post) {
                if (!isset($posted[$post_id])) {
                    $new_posts[$post_id] = $post;
                }
            }
            if(!empty($new_posts)) {
                $json['posts'] = $new_posts;
            }
        }

        exit(json_encode($json));
    }

}